#!/bin/bash
# Free software GPLv3.
# Author: Garcia Sebastian. eldraco@gmail.com

# Script to create a PNG from a RRD file.

DIR=`pwd`
RRD=$1
RRDFILE=$DIR"/"$RRD
OUTPUT_FILE=/tmp/test.png
OUTPUT_FILE=$2

if [ ! $1 ]; then
    echo "Usage: graph_rrd.sh <rrd-file>"
    echo "The rrd-file should be in your same folder"
    exit
fi

echo "Using $RRDFILE"
ETIME=`rrdtool info $RRD |grep  last_update|awk -F"= " '{print $2}'`
TEMP_STIME=`rrdtool dump $RRD |grep -ve "<row><v>NaN</v><v>NaN</v><v>NaN</v><v>NaN</v><v>NaN</v><v>NaN</v><v>NaN</v><v>NaN</v></row>" |grep "<row>" |head -n 1|awk '{print $6}'`
STIME=$(($TEMP_STIME-1000))

/usr/bin/rrdtool graph - --imgformat=PNG --start=$STIME --end=$ETIME --title='Traffic' --base=1000 --height=600 --width=2500 --alt-autoscale-max --lower-limit='0' --vertical-label='' --slope-mode --font TITLE:30: --font AXIS:20: --font LEGEND:20: --font UNIT:7: DEF:udp=$RRDFILE:'UDP':MAX LINE1:udp#00FF00:'UDP' DEF:ipv6=$RRDFILE:'IPV6':MAX LINE1:ipv6#996600:'IPV6' DEF:ssl=$RRDFILE:'SSL':MAX LINE1:ssl#660099:'SSL' DEF:spam=$RRDFILE:'SPAM':MAX LINE1:spam#FFB300:'SPAM' DEF:dns=$RRDFILE:'DNS':MAX LINE1:dns#FFFF00:'DNS' DEF:ssh=$RRDFILE:'SSH':MAX LINE1:ssh#99CCCC:'SSH' DEF:web=$RRDFILE:'WEB':MAX LINE1:web#00FFFF:'WEB' DEF:tcp=$RRDFILE:'TCP':MAX LINE1:tcp#FF0000:'TCP' > $OUTPUT_FILE

echo "You should now display the file in $OUTPUT_FILE"

