#!/bin/bash
# GPLv3 Software
# Author: Garcia Sebastian. eldraco@gmail.com

if [ ! $1 ]; then
    echo "Usage: running_argus_graph.sh <pcap-file>"
    exit
fi


RRDFILE='test.rrd'

# Create the rrd file
# This RRD file is suited to work with our requerimentes and features! modify it if you want.
rrdtool create $RRDFILE --step 60 DS:DNS:GAUGE:3600:0:100000 DS:SPAM:GAUGE:12:0:100000 DS:WEB:GAUGE:120:0:100000 DS:SSL:GAUGE:120:0:100000 DS:SSH:GAUGE:120:0:100000 DS:TCP:GAUGE:120:0:100000 DS:UDP:GAUGE:120:0:100000 DS:IPV6:GAUGE:120:0:100000 RRA:MAX:0.5:1:129600 RRA:MAX:0.5:5:288 RRA:MAX:0.5:30:336 RRA:MAX:0.5:120:372 RRA:MAX:0.5:1440:372

PCAP_FILE=$1

# We need a random port so we can run several instances of this script
PORT=$RANDOM

# CHECK were you haver your argus conf files!
ARGUS_CONF=/etc/argus.conf
RA_CONF=/etc/ra.conf

/usr/bin/argus -F $ARGUS_CONF -r $PCAP_FILE -f -P $PORT  &
PID=$!

# Wait for some traffic to arrive. This is horrible, but it is needed so ra can work fine.
sleep 3

# Debug
#ra -F $RA_CONF -n -Z b -S 127.0.0.1:$PORT | BotSummarizer.py -D -w 1 -r $RRDFILE -f - 
# Without rrdtool
#ra -F $RA_CONF -n -Z b -S 127.0.0.1:$PORT | BotSummarizer.py -w 1 -r $RRDFILE -f - 

# Good one
ra -F $RA_CONF -n -Z b -S 127.0.0.1:$PORT | BotSummarizer.py -w 1 -r $RRDFILE -f - | rrdtool -
kill -9 $PID

